#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/shm.h>
#include <pthread.h>

#define ROW1 6
#define COL2 6

long long faktorial(int n) 
{
    if (n <= 1) return 1LL;
    return (long long)n * faktorial(n - 1);
}

void matrikstranspose(int (*matriks)[COL2], int (*hasil)[ROW1]) 
{
    for (int i = 0; i < COL2; i++) 
    {
        for (int j = 0; j < ROW1; j++) 
        {
            hasil[j][i] = matriks[i][j];
        }
    }
}

void *faktorialTerhitung(void ) 
{
    int (*matriks)[COL2] = (int (*)[COL2]);
    int hasiltranspose[ROW1][COL2];

    matrikstranspose(matriks, hasiltranspose);

    printf("Hasil dari Transpose Matriks:\n");
    for (int i = 0; i < ROW1; i++) 
    {
        for (int j = 0; j < COL2; j++) 
        {
            printf("%d\t", hasiltranspose[i][j]);
        }
        printf("\n");
    }

    long long matriksfaktorial[COL2][ROW1];

    for (int i = 0; i < COL2; i++) 
    {
        for (int j = 0; j < ROW1; j++) 
        {
            matriksfaktorial[i][j] = faktorial(hasiltranspose[i][j]);
        }
    }

    printf("Hasil dari Faktorial Matriks:\n");
    for (int i = 0; i < COL2; i++) 
    {
        for (int j = 0; j < ROW1; j++) 
        {
            printf("%lld\t", matriksfaktorial[i][j]);  
        }
        printf("\n");
    }

    return NULL;
}

int main() 
{
    waktu_mulai, end;
    double waktu_cpu;


    
    start = clock();
    
    int shmid;
    key_t key = ftok("belajar.c", 'R');
    int (*hasilmatriks)[COL2];

    if ((int(*)[COL2]) -1 == (hasilmatriks = shmat(shmid, NULL, 0))) 
    {
        perror("shmat");
        exit(1);
    }

    pthread_t tid;
    pthread_create(&tid, NULL, faktorialTerhitung, hasilmatriks);
    pthread_join(tid, NULL);

 
    shmdt(hasilmatriks);


    end = clock();
    waktu_cpu = ((double) (end - start)) ;

    printf("Waktu eksekusi: %f seconds\n", waktu_cpu);

    return 0;
}


