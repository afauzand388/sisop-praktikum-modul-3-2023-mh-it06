#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

int main() {
    int client_socket;
    struct sockaddr_in server_address;

    client_socket = socket(AF_INET, SOCK_STREAM, 0);// Inisialisasi socket
    if (client_socket == -1) {
        perror("Error in socket");
        exit(1);
    }

    // Konfigurasi alamat server
    server_address.sin_family = AF_INET; //mengatur famili alamat socket ke AF_INET. AF_INET 
    server_address.sin_port = htons(12345); //mengatur nomor port pada struktur server_address. sin_port 
    server_address.sin_addr.s_addr = inet_addr("127.0.0.1"); //mengatur alamat IP pada struktur server_address ke INADDR_ANY. INADDR_ANY

    if (connect(client_socket, (struct sockaddr*)&server_address, sizeof(server_address)) == -1) {// Mencoba terhubung ke server
        perror("Error in connect");
        exit(1);
    }

    // Menerima pesan dari pengguna
    char pesan[1024];  // Buffer untuk pesan
    printf("Masukkan pesan untuk server: ");
    fgets(pesan, sizeof(pesan), stdin);

    send(client_socket, pesan, strlen(pesan), 0); // Mengirim pesan ke server

    close(client_socket);// Menutup koneksi client

    return 0;
}